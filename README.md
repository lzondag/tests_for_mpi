# Example of communication between two subdomains
Domain is 6x6x2, subdomains are 3x6x2 without ghost cells, with ghost cells: 5x6x2.

## Global data:
```
 k= 2
 i= 6          67  68  69  70  71  72
 i= 5          61  62  63  64  65  66
 i= 4          55  56  57  58  59  60
 i= 3          49  50  51  52  53  54
 i= 2          43  44  45  46  47  48
 i= 1          37  38  39  40  41  42

 k= 1
 i= 6          31  32  33  34  35  36
 i= 5          25  26  27  28  29  30
 i= 4          19  20  21  22  23  24
 i= 3          13  14  15  16  17  18
 i= 2           7   8   9  10  11  12
 i= 1           1   2   3   4   5   6
```
## Data in subdomain 1 after distributing
```
 k= 2
 i= 5           0   0   0   0   0   0 <- Ghost cells
 i= 4           0   0   0   0   0   0 <- Ghost cells
 i= 3          49  50  51  52  53  54
 i= 2          43  44  45  46  47  48
 i= 1          37  38  39  40  41  42

 k= 1
 i= 5           0   0   0   0   0   0 <- Ghost cells
 i= 4           0   0   0   0   0   0 <- Ghost cells
 i= 3          13  14  15  16  17  18
 i= 2           7   8   9  10  11  12
 i= 1           1   2   3   4   5   6
```
## Data in subdomain 1 after exchanging information in ghost cells
```
 k= 2
 i= 5          61  62  63  64  65  66 <- Exchanged
 i= 4          55  56  57  58  59  60 <- Exchanged
 i= 3          49  50  51  52  53  54
 i= 2          43  44  45  46  47  48
 i= 1          37  38  39  40  41  42

 k= 1
 i= 5          25  26  27  28  29  30 <- Exchanged
 i= 4          19  20  21  22  23  24 <- Exchanged
 i= 3          13  14  15  16  17  18
 i= 2           7   8   9  10  11  12
 i= 1           1   2   3   4   5   6
```
