module inc_mpi
  use mpi
  implicit none

  integer :: NICV_P, NJCV_P, NKCV_P
  integer :: NI_P, NJ_P, NK_P
  integer :: NIJ_P, NJK_P, NIJK_P
  integer :: NIJCV_P, NJKCV_P, NIJKCV_P
  integer :: NBRSOUTH, NBRNORTH
  integer :: NBRWEST, NBREAST
  integer :: NBRBOTTOM, NBRTOP
  integer :: ierror, num_procs, proc_id, cart_comm, STATUS(MPI_STATUS_SIZE)
  integer :: dims(3) = 0, coords(3)
  integer, dimension(6) :: my_se
  integer, allocatable, dimension(:,:) :: SE ! Array containing start and ends of each proc in global data
  integer :: SCVX, ECVX, SCVY, ECVY, SCVZ, ECVZ ! Start and end control volumes
  integer :: SX, EX, SY, EY, SZ, EZ             ! Start/ends for data in global array
  integer :: SX_P, EX_P, SY_P, EY_P, SZ_P, EZ_P       ! Start/ends for internal data
  integer :: SI_P, EI_P, SJ_P, EJ_P, SK_P, EK_P       ! Start/ends for do loops
  integer :: dataTB !! The datatype to extract/insert the XY-plane ghost cells
  integer :: dataEW !! The datatype to extract/insert the YZ-plane ghost cells
  integer :: dataNS !! The datatype to extract/insert the XZ-plane ghost cells

  integer :: b_t_send, b_t_recv, t_b_send, t_b_recv ! Starting indices when sending/recv from bottom -> top and top -> bottom
  integer :: w_e_send, w_e_recv, e_w_send, e_w_recv
  integer :: s_n_send, s_n_recv, n_s_send, n_s_recv


  contains

  subroutine exchange(A)
    implicit none
    real, dimension(:), intent(inout) :: A


    ! Bottom -> Top
    call MPI_SENDRECV(A(b_t_send), 1, dataTB, NBRTOP, 1, &
                      A(b_t_recv), 1, dataTB, NBRBOTTOM, 1, &
                      cart_comm, status, ierror)

    ! Top -> Bottom
    call MPI_SENDRECV(A(t_b_send), 1, dataTB, NBRBOTTOM, 2, &
                      A(t_b_recv), 1, dataTB, NBRTOP, 2, &
                      cart_comm, status, ierror)

    ! West -> East
    call MPI_SENDRECV(A(w_e_send), 1, dataEW, NBREAST, 3, &
                    A(w_e_recv), 1, dataEW, NBRWEST, 3, &
                    cart_comm, status, ierror)

    ! East -> West
    call MPI_SENDRECV(A(e_w_send), 1, dataEW, NBRWEST, 4, &
                    A(e_w_recv), 1, dataEW, NBREAST, 4, &
                    cart_comm, status, ierror)

    ! South -> North
    call MPI_SENDRECV(A(s_n_send), 1, dataNS, NBRNORTH, 5, &
                    A(s_n_recv), 1, dataNS, NBRSOUTH, 5, &
                    cart_comm, status, ierror)

    ! North -> South
    call MPI_SENDRECV(A(n_s_send), 1, dataNS, NBRSOUTH, 6, &
                    A(n_s_recv), 1, dataNS, NBRNORTH, 6, &
                    cart_comm, status, ierror)
  end subroutine exchange

  subroutine initialize_exchange()
    use inc_data
    implicit none
    integer :: count, blocklength, stride
    integer :: new, sizeof

    b_t_send = NIJ_P * (NK_P - 4) + 1
    b_t_recv = 1
    t_b_send = NIJ_P * 2 + 1
    t_b_recv = NIJ_P * (NK_P - 2) + 1

    w_e_send = NIJ_P - 4 * NJ_P + 1
    w_e_recv = 1
    e_w_send = 2 * NJ_P + 1
    e_w_recv = NIJ_P - 2 * NJ_P + 1

    s_n_send = NJ_P - 4 + 1
    s_n_recv = 1
    n_s_send = 2 + 1
    n_s_recv = NJ_P - 2 + 1

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Make the datatype for the TB Data
    count = 1
    blocklength = 2 * NIJ_P
    stride = blocklength

    call MPI_TYPE_VECTOR(count, blocklength, stride, MPI_REAL, dataTB, ierror)
    call MPI_TYPE_COMMIT(dataTB, ierror)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Make the datatype for the EW Data
    count = NK_P
    blocklength = 2 * NJ_P
    stride = NIJ_P

    call MPI_TYPE_VECTOR(count, blocklength, stride, MPI_REAL, dataEW, ierror)
    call MPI_TYPE_COMMIT(dataEW, ierror)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Make the datatype for the NS Data
    count = NI_P
    blocklength = 2
    stride = NJ_P

    CALL MPI_TYPE_VECTOR (count, blocklength, stride, MPI_REAL, NEW, ierror)
    CALL MPI_TYPE_COMMIT (NEW,ierror)
    CALL MPI_TYPE_EXTENT (MPI_REAL, SIZEOF, ierror)

    count = NK_P
    blocklength = 1
    stride = SIZEOF*NI_P*NJ_P

    CALL MPI_TYPE_HVECTOR (count, blocklength, stride, NEW, DATANS, ierror)
    CALL MPI_TYPE_COMMIT (DATANS, ierror)

    CALL MPI_TYPE_FREE (NEW, ierror )

  end subroutine initialize_exchange

  subroutine distribute(global_data, local_data)
    use inc_data, only: nij, nj
    implicit none
    integer recvsize, sendsize, proc
    integer ssx, sex, ssy, sey, ssz, sez
    integer si, sj, sk, sinp
    integer i,   j,  k, inp
    integer snx, sny, snz
    real, dimension(:), allocatable :: recv, send 
    real, dimension(:), allocatable, intent(inout) :: global_data
    real, dimension(:), allocatable, intent(inout) :: local_data
    integer :: status(MPI_STATUS_SIZE)
    integer, dimension(0:num_procs-1) :: request
    integer :: sendrequest

    recvsize = (EX - SX + 1) * (EY - SY + 1) * (EZ - SZ + 1)
    allocate(recv(recvsize))

    if (proc_id /= 0) call MPI_recv(recv, recvsize, MPI_REAL, 0, proc_id, cart_comm, status, ierror)

    if (proc_id == 0) then
      do proc = 0, num_procs - 1
        ssx = SE(1, proc)
        sex = SE(2, proc)
        ssy = SE(3, proc)
        sey = SE(4, proc)
        ssz = SE(5, proc)
        sez = SE(6, proc)
        snx = (SEX - SSX + 1)
        sny = (SEY - SSY + 1)
        snz = (SEZ - SSZ + 1)
        sendsize = snx * sny * snz
        allocate(send(sendsize))
        k = 0
        do sk = ssz, sez
          k = k + 1
          i = 0
          do si = ssx, sex
            i = i + 1
            j = 0
            do sj = ssy, sey
              j = j + 1
              sinp = (sk - 1) * NIJ + (si - 1) * NJ + sj
              inp  = (k - 1) * snx*sny + (i - 1) * sny + j
              send(inp) = global_data(sinp)
            end do
          end do
        end do
        if (proc == 0) then
          recv = send
        else
          call MPI_send(send, sendsize, MPI_REAL, proc, proc, cart_comm, ierror)
        endif
        deallocate(send)
      end do
    endif
    ! My data is now in my recv variable. Cast it into the local data:
    snx = (EX - SX + 1)
    sny = (EY - SY + 1)
    snz = (EZ - SZ + 1)
    do i = SX_P, EX_P
      si = i - SX_P + 1
      do j = SY_P, EY_P
        sj = j - SY_P + 1
        do k = SZ_P, EZ_P
          sk = k - SZ_P + 1
          inp = (k - 1) * NIJ_P + (i - 1) * NJ_P + j
          sinp = (sk - 1) * (snx * sny) + (si - 1) * (sny) + sj
          local_data(inp) = recv(sinp)
        end do
      end do
    end do

    if (proc_id == 0 .and. allocated(global_data)) deallocate(global_data)

  end subroutine

  subroutine distribute_surface(global_data, local_data, N1, N2, i1, i2)
    implicit none
    real, dimension(:), intent(in) :: global_data
    real, dimension(:), intent(inout) :: local_data
    real, allocatable, dimension(:) :: send
    integer, intent(in) :: N1, N2, i1, i2
    integer :: sendsize, recvsize
    integer :: senda, sendb
    integer :: status(MPI_STATUS_SIZE)
    integer :: a, aa, b, bb, proc
    integer :: s1, e1, s2, e2, size1, size2
    integer :: inp, sinp
    integer, dimension(0:num_procs-1) :: request

    s1 = SE(i1, proc_id)
    e1 = SE(i1+1, proc_id)
    size1 = e1 - s1 + 1
    s2 = SE(i2, proc_id)
    e2 = SE(i2+1, proc_id)
    size2 = e2 - s2 + 1
    recvsize = size1 * size2

    call mpi_Irecv(local_data, recvsize, MPI_REAL, 0, proc_id, cart_comm, request(proc_id), ierror)

    if (proc_id == 0) then 
      do proc = 0, num_procs-1
        s1 = SE(i1, proc)
        e1 = SE(i1+1, proc)
        size1 = e1 - s1 + 1
        s2 = SE(i2, proc)
        e2 = SE(i2+1, proc)
        size2 = e2 - s2 + 1
        sendsize = size1 * size2
        allocate(send(sendsize))
        aa = 0
        do a = s1, e1
          aa = aa + 1
          bb = 0
          do b = s2, e2
            bb = bb + 1
            inp = (b-1) * N1 + a
            sinp = (bb-1) * size1 + aa
            send(sinp) = global_data(inp)
          end do
        end do
        call mpi_send(send, sendsize, MPI_REAL, proc, proc, cart_comm, ierror)
        deallocate(send)
      end do
    endif 

    call MPI_Wait(request(proc_id), status, ierror)

  end subroutine distribute_surface

  subroutine gather_data(global_data, local_data)
    use inc_data, only: nij, nj, nijk
    implicit none
    integer recvsize, sendsize, proc
    integer ssx, sex, ssy, sey, ssz, sez
    integer si, sj, sk, sinp
    integer i,   j,  k, inp
    integer snx, sny, snz
    real, allocatable, dimension(:) :: recv, send 
    real, dimension(:), allocatable, intent(inout) :: global_data
    real, dimension(:), allocatable, intent(in) :: local_data
    integer, dimension(MPI_STATUS_SIZE) :: status
    integer :: alloc_stat


    ! All processors fill their send variable with their local data:
    snx = (EX_P - SX_P + 1)
    sny = (EY_P - SY_P + 1)
    snz = (EZ_P - SZ_P + 1)
    sendsize = snx * sny * snz
    allocate(send(sendsize))

    sk=0
    do k = SZ_P, EZ_P
      sk = sk+1
      si = 0
      do i = SX_P, EX_P
        si = si + 1
        sj = 0
        do j = SY_P, EY_P
          sj = sj + 1
          inp = (k - 1) * NIJ_P + (i - 1) * NJ_P + j
          sinp = (sk - 1) * (snx * sny) + (si - 1) * sny + sj
          send(sinp) = local_data(inp)
        end do
      end do
    end do

    ! The send variable now contains the local data. Send it to the master:
    if (proc_id /= 0)  call MPI_send(send, sendsize, MPI_REAL, 0, proc_id, cart_comm, ierror)
    ! The local variable can now be deallocated

    ! Now the master processor needs to gather these blocks and put it in his global data array.
    ! First check if the global data has already been allocated, if not, allocate it.
    if (proc_id == 0) then
      if (.not. allocated(global_data)) allocate(global_data(nijk))
      do proc = 0, num_procs - 1
        ssx = SE(1, proc)
        sex = SE(2, proc)
        ssy = SE(3, proc)
        sey = SE(4, proc)
        ssz = SE(5, proc)
        sez = SE(6, proc)
        snx = (SEX - SSX + 1)
        sny = (SEY - SSY + 1)
        snz = (SEZ - SSZ + 1)
        recvsize = snx * sny * snz
        allocate(recv(recvsize))
        if (proc /= 0) then
          call MPI_recv(recv, recvsize, MPI_REAL, proc, proc, cart_comm, status, ierror)
        else
          recv = send
        endif
        k = 0
        do sk = ssz, sez
          k = k + 1
          i = 0
          do si = ssx, sex
            i = i + 1
            j = 0
            do sj = ssy, sey
              j = j + 1
              sinp = (sk - 1) * NIJ + (si - 1) * NJ + sj
              inp  = (k - 1) * snx*sny + (i - 1) * sny + j
              global_data(sinp) = recv(inp) 
            end do
          end do
        end do
        if(allocated(recv)) deallocate(recv)
      end do
    end if

    ! call MPI_BARRIER(cart_comm, ierror)
    ! deallocate(local_data)
  end subroutine gather_data

end module inc_mpi
