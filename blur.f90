module inc_blur

implicit none
contains
  subroutine blur(X, NI, NJ, NK, si, ei, sj, ej, sk, ek)
    use inc_mpi, only : proc_id
    implicit none
    real, allocatable, dimension(:), intent(inout) :: X
    real, allocatable, dimension(:) :: Y
    integer I, J, K, II, JJ, KK
    integer, intent(in) :: NI, NJ, NK, si, ei, sj, ej, sk, ek
    integer :: NIJ, NIJK, NJK
    integer XX, YY, ZZ, avg, count
    integer, allocatable, dimension(:,:,:) :: inp

    NIJ = NI * NJ
    NIJK = NIJ * NK
    NJK = NJ * NK
    allocate(Y(SIZE(X)))
    Y = X
    allocate(inp(NI,NJ,NK))
    do K = 1, NK
      do I = 1, NI
        do J = 1, NJ
          inp(I,J,K) = (K-1) * NIJ + (I-1) * NJ + J
        end do
      end do
    end do
    do K = sk, ek
      do I = si, ei
        do J = sj, ej
          X(inp(I,J,K)) = 1.

          count = 0
          avg = 0
          do II = -2, 2
            if ((I + II) < 1 .OR. (I+II) > NI) cycle
            do JJ = -2, 2
              if ((J + JJ) < 1 .OR. (J + JJ) > NJ) cycle
              do KK = -2, 2
                if ((K + KK) < 1 .OR. (K + KK) > NK) cycle
                count = count + 1
                X(inp(I,J,K)) = X(inp(I,J,K)) + Y(inp(I+II,J+JJ,K+KK))
              end do
            end do
          end do
          ! X(inp(I,J,K)) = count
          X(inp(I,J,K)) = X(inp(I,J,K)) / MAX(count,1)
        end do
      end do
    end do

  end subroutine blur
  subroutine calc_uvw(X, NI, NJ, NK, si, ei, sj, ej, sk, ek)
    use inc_mpi, only : proc_id
    implicit none
    real, allocatable, dimension(:), intent(inout) :: X
    real, allocatable, dimension(:) :: Y
    integer I, J, K, II, JJ, KK
    integer, intent(in) :: NI, NJ, NK, si, ei, sj, ej, sk, ek
    integer :: NIJ, NIJK, NJK
    integer XX, YY, ZZ, avg, count
    integer, allocatable, dimension(:,:,:) :: inp

    NIJ = NI * NJ
    NIJK = NIJ * NK
    NJK = NJ * NK
    allocate(Y(SIZE(X)))
    Y = X
    allocate(inp(NI,NJ,NK))
    do K = 1, NK
      do I = 1, NI
        do J = 1, NJ
          inp(I,J,K) = (K-1) * NIJ + (I-1) * NJ + J
        end do
      end do
    end do
    do K = sk, ek
      do I = si, ei
        do J = sj, ej
          X(inp(I,J,K)) = 1./X(inp(I,J,K))
        end do
      end do
    end do

  end subroutine calc_uvw
end module inc_blur