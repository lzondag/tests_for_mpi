PROGRAM mpitests
  use mpi
  use inc_mpi
  use inc_data
  use inc_blur

  implicit none
  integer :: iter, niter = 6
  real, dimension(:), allocatable :: tester


  call bootstrap
  call initialize_exchange

  ! Initialize the global data
  if (proc_id == 0) then
    allocate(XG(NIJK), tester(NIJK))
    do iter = 1,NIJK
      XG(iter) = iter+0.0001
      tester(iter) = iter+0.0001
    end do
  endif
  
  
  call distribute(XG, X)
  !!!!!!!!!!!!!!!!!!!!!!!!! PERFORM COMPUTATION !!!!!!!!!!!!!!!!!!!!!!!!!
  do iter=1,niter
    call exchange(X)

    call calc_uvw(X, NI_P, NJ_P, NK_P, SI_P, EI_P, SJ_P, EJ_P, SK_P, EK_P)
  end do
  !!!!!!!!!!!!!!!!!!!!!!!!! PERFORM COMPUTATION !!!!!!!!!!!!!!!!!!!!!!!!!


  call gather_data(XG, X)
  if (proc_id == 0) then
    do iter=1,niter
      call calc_uvw(tester, NI, NJ, NK, 2, NI-1, 2, NJ-1, 2, NK-1)
    end do
    print*, "Tester equal to the domain decomposition version: ", all(tester == XG)
  endif

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  call MPI_Finalize ( ierror )

END PROGRAM mpitests


subroutine printK(A, ks, ke)
  use inc_mpi
  
  implicit none
  real, dimension(NIJK_P), intent(in) :: A
  integer, intent(in) :: ks, ke
  integer :: k, i, s, e

  print*, "Printing K-surfaces"
  do k = ke, ks, -1
    print*, "k=", k
    do i = NI_P, 1, -1
      s = (k-1) * NIJ_P + (i-1) * NJ_P + 1
      e = (k-1) * NIJ_P + i * NJ_P
      print*,"i=", i, A(s:e)
    end do
    print*,""
  end do


end subroutine

subroutine printKGlobal(A, ks, ke)
  
  use inc_mpi
  use inc_data, only : NIJK, NI, NIJ, NJ
  implicit none
  real, dimension(NIJK), intent(in) :: A
  integer, intent(in) :: ks, ke
  integer :: k, i, s, e

  print*, "Printing K-surfaces"
  do k = ke, ks, -1
    print*, "k=", k
    do i = NI, 1, -1
      s = (k-1) * NIJ + (i-1) * NJ + 1
      e = (k-1) * NIJ + i * NJ
      print*,"i=", i, A(s:e)
    end do
    print*,""
  end do


end subroutine

subroutine bootstrap()
  use mpi
  use inc_mpi
  use inc_data
  implicit none

  logical :: reorder = .TRUE., periods(3) = .FALSE.
  integer :: temp
  !
  !  Initialize MPI.
  !
  call MPI_Init ( ierror )
  call MPI_Comm_size ( MPI_COMM_WORLD, num_procs, ierror )
  call MPI_Comm_rank ( MPI_COMM_WORLD, proc_id, ierror )
  allocate(SE(6,0:num_procs-1))

  call MPI_DIMS_CREATE(num_procs, 3, dims, ierror)
  if (proc_id == 0) print*, dims

  CALL MPI_CART_CREATE (MPI_COMM_WORLD,3,DIMS,PERIODS,REORDER,cart_comm,ierror)

  CALL MPI_CART_SHIFT (cart_comm,0,1,NBRWEST,NBREAST,ierror)
  CALL MPI_CART_SHIFT (cart_comm,1,1,NBRSOUTH,NBRNORTH,ierror)
  CALL MPI_CART_SHIFT (cart_comm,2,1,NBRBOTTOM,NBRTOP,ierror)

  CALL MPI_CART_GET (cart_comm,3,DIMS,PERIODS,COORDS,ierror)

  CALL MPE_DECOMP1D(NICV,DIMS(1),COORDS(1),SCVX,ECVX)
  CALL MPE_DECOMP1D(NJCV,DIMS(2),COORDS(2),SCVY,ECVY)
  CALL MPE_DECOMP1D(NKCV,DIMS(3),COORDS(3),SCVZ,ECVZ)

  call set_ind1d(SCVX, ECVX, NBRWEST, NBREAST, NICV_P, NI_P, SX, EX, SI_P, EI_P, SX_P, EX_P)
  call set_ind1d(SCVY, ECVY, NBRSOUTH, NBRNORTH, NJCV_P, NJ_P, SY, EY, SJ_P, EJ_P, SY_P, EY_P)
  call set_ind1d(SCVZ, ECVZ, NBRBOTTOM, NBRTOP, NKCV_P, NK_P, SZ, EZ, SK_P, EK_P, SZ_P, EZ_P)
  NIJ_P = NI_P * NJ_P
  NJK_P = NJ_P * NK_P
  NIJK_P = NIJ_P * NK_P

  my_se(1)=SX
  my_se(2)=EX
  my_se(3)=SY
  my_se(4)=EY
  my_se(5)=SZ
  my_se(6)=EZ

  if(cart_comm.ne.MPI_COMM_NULL) then
    CALL MPI_ALLGATHER(my_se,6,MPI_INTEGER,SE(1,0),6,MPI_INTEGER,cart_comm,ierror)
  end if

  allocate(X(NIJK_P))

  contains
    subroutine MPE_DECOMP1D(n, numprocs, myid, s, e )
        integer n, numprocs, myid, s, e 
        integer nlocal 
        integer deficit 

        nlocal = n / numprocs 
        s = myid * nlocal + 1 
        deficit = mod(n,numprocs) 
        s = s + min(myid,deficit) 
        if (myid .lt. deficit) then 
          nlocal = nlocal + 1 
        endif 
        e = s + nlocal - 1 
        if (e.gt.n.or.myid.eq.numprocs-1) e = n 
        return 
    end subroutine MPE_DECOMP1D
    subroutine set_ind1d(SCV, ECV, NBR1, NBR2, NCVP, N, S, E, SP, EP, SD, ED)
      !! Based on the start- and ending control volume and the neighbors, determine:
      !! - Number of control volumes NCVP
      !! - Number of datapoints N
      !! - Starting datapoint in global data S
      !! - Ending datapoint in global data E
      !! - Internal starting point for do-loops SP
      !! - Internal end point for do-loops EP
      implicit none
      integer, intent(in) :: SCV, ECV, NBR1, NBR2
      integer, intent (inout) :: N, NCVP, S, E, SP, EP, SD, ED

      if (NBR1 == MPI_PROC_NULL .AND. NBR2 == MPI_PROC_NULL) then ! Domain has no neighbors in this direction
        S = 1 ! This is similar to no decomposition
        E = ECV + 2 ! There are 2 more datapoints than cells
        N = E ! Number of datapoint is simply equal to the ending index
        SP = 2 ! Loops start from 2 if there's no neighbor
        EP = E - 1 ! Loops end 1 before the end
        SD = S ! Internal data spans the entire array
        ED = E ! ^
        NCVP = ECV - SCV + 1 ! Number of control volumes is the same as given from global
      else if (NBR1 == MPI_PROC_NULL .AND. NBR2 /= MPI_PROC_NULL) then ! Domain only has neighbor to its 'right'
        S = 1 ! Data from global starts at 1
        E = ECV + 1 ! Data from global ends 1 index further than the CV's number
        SP = 2 ! Loop starts from 2 to skip boundary value
        EP = E ! Ends at the last data index because this is still an internal value. 2 more ghost values will be added to its right
        SD = 1 ! Internal data starts at 1
        ED = E ! Internal data stops at E
        N = (ED - SD + 1) + 2 ! Number of internal datapoint + 2 ghost values
        NCVP = ECV - SCV + 1 + 2 ! Number of control volumes given + 2 ghost cells
      else if (NBR1 /= MPI_PROC_NULL .AND. NBR2 == MPI_PROC_NULL) then ! Domain only has neighbor to its 'left'
        S = SCV + 1 ! Data starts at the cell's number + 1 offset from boundary value
        E = ECV + 2 ! Data ends at the boundary value (CV's number + 2 offset from both boundary values)
        SP = 3 ! Loop starts from first internal value
        EP = 2 + (ECV - SCV + 1) ! Loop ends at the end - 1
        SD = 3
        ED = 2 + (ECV - SCV + 1) + 1
        N = ED - SD + 1 + 2 ! All internal data points + 2 ghost values.
        NCVP = ECV - SCV + 1 + 2 ! Number of control volumes given + 2 ghost cells
      else if (NBR1 /= MPI_PROC_NULL .AND. NBR2 /= MPI_PROC_NULL) then !Domain has neighbors to its 'left' and 'right
        S = SCV + 1 ! 1 offset from boundary value
        E = ECV + 1 ! 1 offset from boundary value
        SP = 3 ! Start loop from first internal value, skipping the 2 ghost values
        EP = (E - S + 1) + 2 ! End at: number of internal datapoints + 2 ghost points
        SD = SP ! First internal datavalue, after ghost values
        ED = EP ! Last internal datavalue, before ghost values
        N = ED - SD + 1 + 2 + 2
        NCVP = ECV - SCV + 1 + 4 ! Number of control volumes given + 4 ghost cells
      endif

    end subroutine set_ind1d
end subroutine
