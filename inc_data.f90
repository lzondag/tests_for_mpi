module inc_data
  
  implicit none
  real, allocatable, dimension(:) :: X
  real, allocatable, dimension(:) :: XG
  integer :: I, J, K
  integer, allocatable, dimension(:,:,:) :: INP
  save
  integer, parameter :: NICV = 50, NJCV = 60, NKCV = 70
  integer, parameter :: NI = NICV + 2, NJ = NJCV + 2, NK = NKCV + 2
  integer, parameter :: NIJ = NI * NJ, NJK = NJ * NK, NIJK = NIJ * NK
end module inc_data